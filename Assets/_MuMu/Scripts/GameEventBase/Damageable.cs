using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Damageable : MonoBehaviour
{
    public float maxHealth;

    public UnityEvent Damaged;
    public UnityEvent Died;

    public float currentHealth;
    private void OnEnable() => currentHealth = maxHealth;

    public void Damage(float damage)
    {
        if (currentHealth <= 0) return;
        Damaged.Invoke();
        currentHealth -= damage;

        if (currentHealth <= 0)
            Die();
    }

    private void Die()
    {
        Died.Invoke();
    }
}
