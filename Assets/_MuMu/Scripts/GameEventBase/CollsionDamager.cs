using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CollsionDamager : Damager
{
    private void OnCollisionEnter(Collision collision)
    {
        Damageable damageable = collision.collider.GetComponent<Damageable>();

        if (damageable)
            damageable.Damage(damage);
    }
}
