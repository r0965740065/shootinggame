using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEventListener : MonoBehaviour
{
    public GameEvent _event;
    public UnityEvent _response;

    private void OnEnable() => _event.Add(this);
    private void OnDisable() => _event.Remove(this);

    public void Invoke() => _response.Invoke();
}
