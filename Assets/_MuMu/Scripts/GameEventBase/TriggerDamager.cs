using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDamager : Damager
{
    private void OnTriggerEnter(Collider other)
    {
        Damageable damageable = other.GetComponent<Damageable>();

        if (damageable)
            damageable.Damage(damage);
    }
}
