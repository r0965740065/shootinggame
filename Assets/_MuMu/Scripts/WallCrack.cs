using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WallCrack : MonoBehaviour
{
    private Damageable damageable;

    private void Start()
    {
        damageable = GetComponent<Damageable>();
    }

    public void Crack()
    {
        if(damageable.currentHealth <= damageable.maxHealth / (20 / 100))
        {

        }
    }

    public void Destroy()
    {

    }
}
