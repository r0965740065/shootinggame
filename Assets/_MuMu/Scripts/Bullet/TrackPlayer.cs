using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class TrackPlayer : MonoBehaviour
{
    private Rigidbody rb;

    public Position _targetPosition;
    public float force;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        Vector3 dir = MathfTool.CountDirNoY(transform.position, _targetPosition.Value);
        rb.AddForce(dir * force);
    }
}
