using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EPS;

public class Spawner : MonoBehaviour
{
    public virtual void Spawn(PoolObject poolObject)
    {
        PoolManager.Spawn(poolObject, transform.position, transform.rotation);
    }
}
