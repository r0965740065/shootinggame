﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

namespace EPS
{
	[RequireComponent(typeof(ParticleSystem))]
	public class PoolVFX : PoolObject
	{
		public float recycleTime = 0.1f;

		private ParticleSystem ps;
		private void Awake() => ps = GetComponent<ParticleSystem>();

	 	private void Start() => StartCoroutine(Recycle());

		private IEnumerator Recycle()
		{
			yield return new WaitForSeconds(recycleTime);
			Deactivate();
		}

		/// <summary>
		/// Active state of the object
		/// </summary>
		public override bool Active
		{
			get => ps.isPlaying;

			protected set
			{
				if(value)
					ps.Play();
				else
					ps.Stop();
			}
		}
	}
}
