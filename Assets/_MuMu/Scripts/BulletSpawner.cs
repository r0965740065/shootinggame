using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EPS;

public class BulletSpawner : MonoBehaviour
{
    public Transform muzzleTransform;

    public void Spawn(PoolObject poolObject)
    {
        PoolManager.Spawn(poolObject, muzzleTransform.position, muzzleTransform.rotation);
    }
}
