using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EPS;

public class PlayerShield : MonoBehaviour
{
    public PoolObject shield;
    public int maxShieldCount;
    private int _currentShieldCount;
    public int currentShieldCount
    {
        get { return _currentShieldCount; }
        set
        {
            if (value > maxShieldCount)
                _currentShieldCount = maxShieldCount;
            else if (value < 0)
                _currentShieldCount = 0;
            else
                _currentShieldCount = value;
        }
    }

    public float radius;
    public float angle;
    private float perAngle { get { return angle / maxShieldCount; } }

    private Transform shieldCenterParent;
    private List<PoolObject> currentShield = new List<PoolObject>();

    private void Start()
    {
        currentShieldCount = maxShieldCount;
        SpawnShieldCenter();
        AddShield(currentShieldCount);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.O))
            AddShield(1);
        else if (Input.GetKeyDown(KeyCode.P))
            ReduceShield(2);
    }

    private void SpawnShieldCenter()
    {
        shieldCenterParent = new GameObject("ShieldCenter").transform;
        shieldCenterParent.parent = transform;
        shieldCenterParent.position = transform.position;
        shieldCenterParent.rotation = transform.rotation;
    }

    public void AddShield(int amount)
    {
        currentShieldCount += amount;
        SpawnShield();
    }

    public void ReduceShield(int amount)
    {
        currentShieldCount -= amount;
        DestroyShield();
    }

    private void SpawnShield()
    {
        int count = currentShieldCount - currentShield.Count;
        for (int i = 0; i < count; i++)
        {
            PoolObject poolObject = PoolManager.Spawn(shield, shieldCenterParent);
            currentShield.Add(poolObject);
        }
        RefreshShieldPosition();
    }

    private void DestroyShield()
    {
        int count = currentShield.Count - currentShieldCount;
        for (int i = 0; i < count; i++)
        {
            currentShield[0].Deactivate();
            currentShield.RemoveAt(0);
        }
        RefreshShieldPosition();
    }

    private void RefreshShieldPosition()
    {
        float allAngle = (currentShieldCount - 1) * perAngle;
        float startAngle = -allAngle / 2;
        for(int i = 0; i < currentShieldCount; i++)
        {
            currentShield[i].transform.position = shieldCenterParent.position + Quaternion.Euler(0, startAngle + i * perAngle, 0) * shieldCenterParent.forward * radius;
            currentShield[i].transform.forward = Quaternion.Euler(0, startAngle + i * perAngle, 0) * shieldCenterParent.forward;
        }
    }
}
