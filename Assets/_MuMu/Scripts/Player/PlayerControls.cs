using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{
    private Rigidbody rb;

    public Vector3 input;
    public float moveForce;
    public float maxSpeed;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        Move();
        LookMousePosition();
    }

    private void Move()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        input = new Vector3(h, 0, v);

        rb.AddForce(input.normalized * moveForce);
        Mathf.Clamp(rb.velocity.magnitude, 0, maxSpeed);
    }

    private void LookMousePosition()
    {
        Ray cameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        Physics.Raycast(cameraRay, out RaycastHit hit, 100, LayerMask.GetMask("Ground"));

        if (hit.collider == null) return;
        transform.forward = MathfTool.CountDirNoY(transform.position, hit.point);
    }
}
