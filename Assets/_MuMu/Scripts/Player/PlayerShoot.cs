using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerShoot : MonoBehaviour
{
    public UnityEvent OnShoot;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
            OnShoot.Invoke();
    }
}
