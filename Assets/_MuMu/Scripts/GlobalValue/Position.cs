using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Global Variables/Positon")]
public class Position : ScriptableObject
{
    public Vector3 Value { get; set; }
}
