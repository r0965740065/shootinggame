using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Global Variables/Float")]
public class Float : ScriptableObject
{
    public float Value;
}
