using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnCollsionEvent : MonoBehaviour
{
    public UnityEvent OnCollision;

    private void OnCollisionEnter(Collision collision)
    {
        OnCollision.Invoke();
    }
}
