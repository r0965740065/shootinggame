using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionWriter : MonoBehaviour
{
    public Position _position;

    private void Update() => _position.Value = transform.position;
}
