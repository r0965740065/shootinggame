using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathfTool
{
    public static Vector3 CountDirNoY(Vector3 a, Vector3 b)
    {
        Vector3 dir = b - a;
        dir.y = 0;
        return dir.normalized;
    }
}
